#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guozg
@File：test_data.py
"""
import pytest
import os
import yaml
import allure
from page.demo.get_child_list import GetChild
from common.yml.get_yml_data import GetYmlData
from common.util.auto_assert import AutoAssert
get_yml_data = GetYmlData()
yml_dir = get_yml_data.get_yml_dir()
# case对应的yaml文件所在的目录(注意不能包含data目录)
case_yml_dir = 'demo'
yml_name = 'test_data.yml'
if case_yml_dir == None:
    yml_path = os.path.join(yml_dir,yml_name)
else:
    yml_path = os.path.join(os.path.join(yml_dir,case_yml_dir),yml_name)
login_data =[]
login_data.append(yaml.safe_load(open(yml_path,mode='r',encoding='utf-8'))['test_init'])
login_res = None

autoassert= AutoAssert()

@allure.feature("api接口测试框架演示")
class TestData:


    @pytest.mark.parametrize("get_app_login_response",login_data,indirect=True)
    @pytest.mark.get_login_res
    @allure.story("class下的全局登录")
    @allure.step("调用conftest.py,初始化登录,并保存相应的token")
    @allure.severity("blocker")
    def test_init(self,get_app_login_response):
        ''''''
        print("初始化登录,保存相应的token")

    @allure.story("获取班级列表")
    @allure.step("先获取case对应的yml文件的值")
    @pytest.mark.get_classes
    def test_classes(self):
        yml_data = GetYmlData().get_first_call_yml_data(case_yml_dir)
        print(f"case 对应的yml文件的值为:\n{yml_data}")
        with allure.step("执行该接口"):
            res = GetChild().get_classes(yml_data)

        with allure.step("进行自动断言"):
            autoassert.auto_assert(yml_data,res)
            assert 1<2

    @allure.story("获取孩子列表")
    @allure.description("这是一个case的描述,现在放在case头部位置")
    def test_child(self):
        yml_data = GetYmlData().get_first_call_yml_data(case_yml_dir)
        res =GetChild().get_child(params=yml_data)
        autoassert.auto_assert(yml_data,res)

    def test_stayover(self):
        yml_data = GetYmlData().get_first_call_yml_data(case_yml_dir)
        res =GetChild().get_stayover(yml_data)

    @allure.story("获取待办")
    def test_daiban(self):
        yml_data = GetYmlData().get_first_call_yml_data(case_yml_dir)
        res =GetChild().get_daiban(yml_data)

    @allure.story("请假列表")
    def test_leavelist(self):
        yml_data = GetYmlData().get_first_call_yml_data(case_yml_dir)
        res =GetChild().get_leavelist(yml_data)

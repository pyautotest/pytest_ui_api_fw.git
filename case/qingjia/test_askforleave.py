#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：test_askforleave.py
"""


import pytest
import os
import yaml
import allure

from case.test_data import autoassert
from page.qingjia.add_askforleave import AskForLeave
from common.yml.get_yml_data import GetYmlData
from common.util.auto_assert import AutoAssert
get_yml_data = GetYmlData()
yml_dir = get_yml_data.get_yml_dir()
# case对应的yaml文件所在的目录(注意不能包含data目录)
case_yml_dir = 'qingjia'
# case 对应的yml文件名称
yml_name = 'test_askforleave.yml'
if case_yml_dir == None:
    yml_path = os.path.join(yml_dir,yml_name)
else:
    yml_path = os.path.join(os.path.join(yml_dir,case_yml_dir),yml_name)
login_data =[]
login_data.append(yaml.safe_load(open(yml_path,mode='r',encoding='utf-8'))['test_init'])
login_res = None



@allure.feature("web端--请假全流程接口测试")
class TestAskforLeave:


    @pytest.mark.parametrize('get_web_login_response',login_data,indirect=True)
    def test_init(self,get_web_login_response):
        res = get_web_login_response
        with allure.step("将登录的结果赋值给全局变量,供该class下的其他case使用"):
            pass

    def test_add(self):
        yml_data = GetYmlData().get_first_call_yml_data(case_yml_dir)
        print(f"case层 对应的yml文件的值如下:\n{yml_data}")
        with allure.step("执行该接口"):
            res = AskForLeave().add(yml_data)
        autoassert.auto_assert(yml_data, res)

    def test_query(self):
        yml_data= GetYmlData().get_first_call_yml_data(case_yml_dir)
        # print(f"case层 对应的yml文件的值如下:\n{yml_path}")
        with allure.step("执行请假查询接口,并进行数据校验"):
            res = AskForLeave().query(yml_data)

        autoassert.auto_assert(yml_data,res)

    def test_deldata(self):
        yml_data = GetYmlData().get_first_call_yml_data(case_yml_dir)
        print(f"case层 对应的yml文件的值如下:\n{yml_path}")
        with allure.step("删除请假数据"):
            res = AskForLeave().deldata(yml_data)

        autoassert.auto_assert(yml_data, res)



#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guozg
@File：login.py
"""
import requests
import sys
from string import Template
import json

sys.path.append('../')
from common.cookie.get_web_cookie import GetWebCookie
from common.cookie.get_app_cookie_header import GetAppCookieHeader
from common.util.generate_device_key import GenerateDeviceKey
from config.get_conf_data import GetConfData
from common.util.handle_jsonfile import HandleJsonFile


class Login:
    '''
    获取token
    '''

    def __init__(self):
        self.__conf = GetConfData()
        self.__web_url = self.__conf.get_web_login_url()
        self.__app_url = self.__conf.get_app_login_url()
        self.__web_request_data = self.__conf.get_web_login_request_data()
        self.__app_request_data = self.__conf.get_app_login_request_data()
        self.__data_key = self.__conf.get_data_key()
        self.__loginId_key = self.__conf.get_loginId_key()
        self.__device_key = self.__conf.get_device_key()
        self.__hjson = HandleJsonFile()

    def get_web_token(self, request_data: dict, token_path: str = None) -> dict:
        '''
        获取web端的token,并返回. 最后的格式为一个字典  \n
        在使用该token时,可以使用cookies=token即可.  \n
        :param request_data: 登录的请求参数,形如{username:18342100001,password:test1234}
        :param token_path: 保存的token的路径.
        :return: cookie{login: XXXXXX}
        '''
        # 使用模板技术,进行变量替换
        req_data = self.__replace_data(self.__web_request_data, self.__web_url, request_data)
        # 进行请求.
        req = requests.request(**req_data)
        # 获取相应的cookie
        token = GetWebCookie(req).get_cookie()
        # 将token保存到json文件里.
        if token_path !=None:
            self.__hjson.write_dict_to_json_file(token_path,token)

        return token

    def get_web_login_response(self, request_data: dict):
        '''
        获取web端的token,并返回. 最后的格式为一个字典  \n
        在使用该token时,可以使用cookies=token即可.  \n
        :param request_data: 登录的请求参数,形如{username:18342100001,password:test1234}
        :param token_path: 保存的token的路径
        :return: response
        '''
        # 使用模板技术,进行变量替换
        req_data = self.__replace_data(self.__web_request_data, self.__web_url, request_data)
        # 进行请求.
        req = requests.request(**req_data)
        return req

    def get_app_token(self, request_data: dict, token_path: str = None) -> dict:
        '''
        获取app端的token,并返回.  \n
        :param request_data: 登录的请求参数,形如{loginId:18342100001,password:1234}
        :return: header,形如{user:34212,session: XXXXXX}
        '''
        # 使用模板技术,进行变量替换
        req_data = self.__replace_data(self.__app_request_data, self.__app_url, request_data)
        # 获取loginid,用于生成device
        login_id = req_data[self.__data_key][self.__loginId_key]
        # 生成相应的设备号
        req_data[self.__data_key][self.__device_key] = GenerateDeviceKey().generate_key_by_account(login_id)
        # 进行请求
        req = requests.request(**req_data)
        # 获取token
        token = GetAppCookieHeader(req).get_header()
        # 将token保存到json文件里
        if token_path !=None:
            self.__hjson.write_dict_to_json_file(token_path,token)

        return token

    def get_app_login_response(self, request_data: dict):
        '''
        获取app端的token,并返回.  \n
        :param request_data: 登录的请求参数,形如{loginId:18342100001,password:1234}
        :return: response
        '''
        # 使用模板技术,进行变量替换
        req_data = self.__replace_data(self.__app_request_data, self.__app_url, request_data)
        # 获取loginid,用于生成device
        login_id = req_data[self.__data_key][self.__loginId_key]
        # 生成相应的设备号
        req_data[self.__data_key][self.__device_key] = GenerateDeviceKey().generate_key_by_account(login_id)
        # 进行请求
        req = requests.request(**req_data)
        return req

    def __replace_data(self, template, url: str, data: dict) -> dict:
        '''
        使用模板替换技术,方便快捷.  \n
        就是将config.yml文件里的变量 全部进行替换掉.替换后的dict 是可以直接进行请求  \n
        :param template: 为读取的yml文件,转化为string的dict
        :param url: url地址,因为url也需要进行替换.
        :param data: 外部传入的参数值,为字典类型,如{loginId:18342100001,password:test1234}
        :return: 替换后的字典,可用于直接请求
        '''
        # 使用模板(注意,必须将dict转化为string)
        data_str = Template(json.dumps(template))
        # 将变量进行替换为具体的值.
        replace_value = data_str.substitute(url=url, **data)
        # 将替换后的string类型的dict,再转换为dict,并返回
        return json.loads(replace_value)


if __name__ == '__main__':
    web_login_data = {
        'username': 18342100004,
        'password': "test1234"
    }

    app_login_data = {
        'type': 1,
        'loginId': "15342100001",
        'password': "1234",
        'apptype': 1,  # 1android 2ios
        'appBusType': 1,  # 1:家长端   2：教师端
        'device': ""
    }
    app_login_data2 = {
        'type': 1,
        'loginId': "18342100005",
        'password': "1234",
        'apptype': 1,  # 1android 2ios
        'appBusType': 1,  # 1:家长端   2：教师端
        'device': ""
    }

    test = Login()
    print(test.get_web_token(web_login_data))
    print(test.get_app_token(app_login_data))
    print(test.get_app_token(app_login_data2))

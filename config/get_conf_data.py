#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：get_conf_data.py
"""

import yaml
import os

from config.config_yml_keys_mapping import ConfYmlKeys

cur_dir = os.path.dirname(os.path.realpath(__file__))
yml_name = "config.yml"
yml_path = os.path.join(cur_dir, yml_name)
'''
要将一些配置都放在config里，如 yml文件的目录等
'''


class GetConfData(ConfYmlKeys):

    def __init__(self):
        super(GetConfData, self).__init__()
        # 获取config.yml中key 对应的值。
        self.__yml_data = yaml.safe_load(open(yml_path, mode='r', encoding="utf-8"))
        self.__host = self.__yml_data[self.host_key]
        self.__unify = self.__yml_data[self.unify_host_key]
        self.__web_login_path = self.__yml_data[self.web_login_path_key]
        self.__app_login_path = self.__yml_data[self.app_login_path_key]
        self.__web_url = self.__host + self.__web_login_path
        self.__app_url = self.__host + self.__app_login_path
        self.__web_request_data = self.__yml_data[self.web_request_data_key]
        self.__app_request_data = self.__yml_data[self.app_request_data_key]
        self.__case_login_name = self.__yml_data[self.case_login_name_key]

    # -----------------------下面为常用的yml文件中的值---------------------------
    def get_config_yml_data(self):
        """获取config.yml文件的数据"""
        return self.__yml_data

    def get_unify_host(self) -> bool:
        '''默认值为True，即统一环境，从config.yml文件里读取'''
        res = True
        if self.__unify == False:
            res = False
        return res

    def get_host(self) -> str:
        '''获取host地址'''
        return self.__host

    def get_web_login_url(self) -> str:
        '''获取web端的登录地址'''
        return self.__web_url

    def get_app_login_url(self) -> str:
        '''获取app端的登录地址'''
        return self.__app_url

    def get_case_login_name(self) -> str:
        """获取每个class类中的全局的获取登录结果的函数名称"""
        return self.__case_login_name

    def get_web_login_request_data(self) -> str:
        '''获取web端的登录请求参数'''
        return self.__replace(self.__web_request_data, self.__web_url)

    def get_app_login_request_data(self) -> str:
        '''获取app端的登录请求参数'''
        return self.__replace(self.__app_request_data, self.__app_url)

    # ----------------------下面为key的返回值------------------------------
    def get_host_key(self):
        """获取host 的key的名称"""
        return self.host_key

    def get_url_key(self):
        """获取url 的key的名称"""
        return self.url_key

    def get_unify_host_key(self):
        """获取 unify_host 的key的名称"""
        return self.unify_host_key

    def get_web_login_path_key(self):
        """获取web_login_path 的key的名称"""
        return self.web_login_path_key

    def get_app_login_path_key(self):
        """获取app_login_path 的key的名称"""
        return self.app_login_path_key

    def get_web_request_data_key(self):
        """获取web_request_data 的key的名称"""
        return self.web_request_data_key

    def get_app_request_data_key(self):
        """获取app_request_data 的key的名称"""
        return self.app_request_data_key

    def get_case_login_name_key(self):
        """获取case_login_name 的key的名称"""
        return self.case_login_name_key

    def get_data_key(self):
        """获取data 的key的名称"""
        return self.data_key

    def get_type_key(self):
        """获取type 的key的名称"""
        return self.type_key

    def get_loginType_key(self):
        """获取loginType 的key的名称"""
        return self.loginType_key

    def get_username_key(self):
        """获取username 的key的名称"""
        return self.username_key

    def get_password_key(self):
        """获取password 的key的名称"""
        return self.password_key

    def get_code_key(self):
        """获取code 的key的名称"""
        return self.code_key

    def get_loginId_key(self):
        """获取loginId 的key的名称"""
        return self.loginId_key

    def get_device_key(self):
        """获取device 的key的名称"""
        return self.device_key

    # -------------- yml 文件 常用的值的key ----------------
    def get_api_str_key(self):
        """获取 api_str 的key的名称"""
        return self.api_str_key

    def get_local_str_key(self):
        """获取 local_str 的key的名称"""
        return self.local_str_key

    def get_global_str_key(self):
        """获取 global_str 的key的名称"""
        return self.global_str_key

    def get_app_str_key(self):
        """获取 app_str 的key的名称"""
        return self.app_str_key

    def get_web_str_key(self):
        """获取 web_str 的key的名称"""
        return self.web_str_key

    def get_request_str_key(self):
        """获取 request_str 的key的名称"""
        return self.request_str_key

    def get_response_str_key(self):
        """获取 response_str 的key的名称"""
        return self.response_str_key

    def get_login_str_key(self):
        """获取 login_str 的key的名称"""
        return self.login_str_key

    def get_test_init_key(self):
        """获取 test_init 的key的名称"""
        return self.test_init_key

    # ------------- yml 文件中 常见的值 ----------------
    def get_api_str(self):
        """获取 'api' 字符串"""
        return self.__yml_data[self.api_str_key]

    def get_local_str(self):
        """获取 'local' 字符串"""
        return self.__yml_data[self.local_str_key]

    def get_global_str(self):
        """获取 'global' 字符串"""
        return self.__yml_data[self.global_str_key]

    def get_app_str(self):
        """获取 'app' 字符串"""
        return self.__yml_data[self.app_str_key]

    def get_web_str(self):
        """获取 'web' 字符串"""
        return self.__yml_data[self.web_str_key]

    def get_request_str(self):
        """获取 'request' 字符串"""
        return self.__yml_data[self.request_str_key]

    def get_response_str(self):
        """获取 'response' 字符串"""
        return self.__yml_data[self.response_str_key]

    def get_login_str(self):
        """获取 'login' 字符串"""
        return self.__yml_data[self.login_str_key]

    def get_yml_str(self):
        """获取 'yml' 字符串"""
        return self.__yml_data[self.yml_str_key]

    def get_json_str(self):
        """获取 'json' 字符串"""
        return self.__yml_data[self.json_str_key]

    def get_sql_str(self):
        """获取 'sql' 字符串"""
        return self.__yml_data[self.sql_str_key]

    def get_all_str(self):
        """获取 'all' 字符串"""
        return self.__yml_data[self.all_str_key]

    def get_second_str(self):
        """获取 'second' 字符串"""
        return self.__yml_data[self.second_str_key]

    def get_minute_str(self):
        """获取 'minute' 字符串"""
        return self.__yml_data[self.minute_str_key]

    def get_hour_str(self):
        """获取 'hour' 字符串"""
        return self.__yml_data[self.hour_str_key]

    def get_day_str(self):
        """获取 'day' 字符串"""
        return self.__yml_data[self.day_str_key]



    def __replace(self, request_data, url) -> dict:
        '''替换数据'''
        request_data[self.url_key] = url
        return request_data

    # -----获取全局登录结果的方法的名称------------
    def get_test_init_func_name(self):
        """每个class下 获取登录结果的方法名称"""
        return self.__yml_data[self.test_init_key]

    def get_login_func_name(self):
        """每个class下 获取登录结果的方法名称"""
        return self.get_test_init_func_name()

    # -------------- 获取 case数据层及page业务层 yml文件所在的目录 --------------
    def get_yml_dir_key(self):
        """"""
        return self.yml_dir_key

    def get_handled_yml_dir_key(self):
        """"""
        return self.handled_yml_dir_key

    def get_slash_key(self):
        """"""
        return self.slash_key

    def get_slash_value(self):
        """获取斜杠的值"""
        return self.__yml_data[self.slash_key]

    def get_backslash_key(self):
        """"""
        return self.backslash_key

    def get_backslash_value(self):
        """获取反斜杠的值"""
        return self.__yml_data[self.backslash_key]

    def get_proj_path(self):
        """获取项目的根目录路径"""
        return os.path.dirname(cur_dir)

    def get_proj_abspath(self):
        """获取项目的根目录的绝对路径"""
        return os.path.abspath(self.get_proj_path())

    def get_yml_dir(self):
        """
        获取config.yml文件中设置的case及page 所涉及到的yml文件所在的目录名称
        如果目录名称里有斜杠或反斜杠,要过滤掉
        """
        yml_dir = self.__yml_data[self.yml_dir_key]
        return self.__remove_slash(yml_dir)

    def get_ymlfile_path(self):
        """获取case及page所涉及到的yml文件的所在目录的相对路径"""
        # 在进行路径拼接时，要检查yml_dir的值是否是以(反)斜杠开头或结尾
        ymlfile_dir = self.get_yml_dir()
        ymlfile_path = os.path.join(self.get_proj_path(), ymlfile_dir)
        return ymlfile_path

    def get_ymlfile_abspath(self):
        """获取case及page所涉及到的yml文件的所在目录的绝对路径"""
        return os.path.abspath(self.get_ymlfile_path())

    def get_handled_yml_dir(self):
        """获取处理后的yml文件保存的目录(该目录最终会保存在ymlfile目录下),不含ymlfile目录."""
        handled_yml_dir = self.__yml_data[self.handled_yml_dir_key]
        return self.__remove_slash(handled_yml_dir)

    def get_handled_yml_path(self):
        """获取handled_yml的路径(拼接后的,含ymlfile目录)"""
        handled_yml_dir = self.get_handled_yml_dir()
        ymlfile_path = self.get_ymlfile_path()
        handled_yml_path = os.path.join(ymlfile_path,handled_yml_dir)
        return handled_yml_path

    def get_handled_yml_abspath(self):
        """获取处理后的yml文件保存的绝对路径(拼接后的,含ymlfile目录)"""
        return os.path.abspath(self.get_handled_yml_path())


    # ----------------- 依赖数据 相关的值 -------------------
    def get_caseid_key(self):
        """获取caseid 的key的名称"""
        return self.caseid_key

    def get_caseid(self):
        """获取caseid key(依赖数据list中的dict的key)所对应的值"""
        return self.__yml_data[self.caseid_key]

    def get_filetype_key(self):
        """获取filetype 的key的名称"""
        return self.filetype_key

    def get_filetype(self):
        """获取filetype key 所对应的值"""
        return self.__yml_data[self.filetype_key]

    def get_filedata_key(self):
        """获取filedata 的key的名称"""
        return self.filedata_key

    def get_filedata(self):
        """获取filedata key(依赖数据list中的dict的key)所对应的值"""
        return self.__yml_data[self.filedata_key]

    def get_testinit_key(self):
        """获取testinit 的key的名称"""
        return self.testinit_key

    def get_testinit(self):
        """获取testinit key(依赖数据list中的dict的key)所对应的值"""
        return self.__yml_data[self.testinit_key]

    # ---------- case_id case层与page层之间的连接的标志等 --------
    def get_case_join_page_key(self):
        """获取 case_join_page 的key的名称"""
        return self.case_join_page_key

    def get_case_join_page(self):
        """获取 case_join_page 对应的值"""
        return self.__yml_data[self.case_join_page_key]

    def get_yml_join_func_key(self):
        """获取 yml_join_func 的key的名称"""
        return self.yml_join_func_key

    def get_yml_join_func(self):
        """获取 yml_join_func 对应的值"""
        return self.__yml_data[self.yml_join_func_key]

    def get_key_join_key(self):
        """获取 key_join 对应的key的名称"""
        return self.key_join_key

    def get_key_join(self):
        """获取 key_join 对应的值"""
        return self.__yml_data[self.key_join_key]

    # ----------------保存接口执行结果 jsonfile 的目录 及其他相关的操作 --------
    def get_jsonfile_dir(self):
        """
        获取jsonfile_dir 对应的值,如果有斜杠或反斜杠,要过滤掉
        """
        jsonfile_dir = self.__yml_data[self.jsonfile_dir_key]
        return self.__remove_slash(jsonfile_dir)

    def get_unify_savevalue(self):
        """是否统一保存运行结果"""
        return self.__yml_data[self.unify_savevalue_key]

    def get_savetype(self):
        """保存结果的类型，json、sql、all(json+sql)"""
        return self.__yml_data[self.save_type]

    def get_jsonfile_path(self):
        """获取保存jsonfile(接口的执行结果,以及一些查询后的校验结果)的路径,去掉开头和结尾的斜杠和反斜杠"""
        jsonfile_dir = self.get_jsonfile_dir()
        # 过滤掉开头和结尾的斜杠与反斜杠
        jsonfile_dir = os.path.join(self.get_proj_path(), jsonfile_dir)
        return jsonfile_dir

    def get_jsonfile_abspath(self):
        """获取保存jsonfile(接口的执行结果,以及一些查询后的校验结果)的绝对路径"""
        return os.path.abspath(self.get_jsonfile_path())

    def get_web_session_limit(self):
        """获取web_session_limit 对应的值"""
        return self.__yml_data[self.websession_limit_key]

    def get_app_session_limit(self):
        """获取app_session_limit 对应的值"""
        return self.__yml_data[self.appsession_limit_key]

    def get_android_session_limit(self):
        """获取android_session_limit 对应的值"""
        return self.__yml_data[self.android_session_limit_key]

    def get_ios_session_limit(self):
        """获取ios_session_limit 对应的值"""
        return self.__yml_data[self.ios_session_limit_key]


    def __remove_slash(self,dir:str):
        """移除斜杠和反斜杠"""
        # 获取斜杠的值/
        slash = self.get_slash_value()
        # 获取反斜杠\\的值
        backslash = self.get_backslash_value()
        dir = dir.strip(slash).strip(backslash)
        return dir



if __name__ == '__main__':
    # print(GetConfData().get_global_str())
    # print(GetConfData().login_str_key)
    # print(GetConfData().get_host())
    # print(GetConfData().get_login_str())
    # print(GetConfData().get_ymlfile_abspath())
    ymlpath = GetConfData().get_ymlfile_abspath()
    print(os.path.join(ymlpath, "opt/test_data.yml"))
    print(GetConfData().get_jsonfile_dir())
    print(GetConfData().get_jsonfile_abspath())
    print(GetConfData().get_ymlfile_path())
    print(GetConfData().get_unify_savevalue())
    print(GetConfData().get_savetype())
    print(type(GetConfData().get_android_session_limit()))

# pytest_ui_api_fw

#### 介绍

1. 该框架是基于pyest、requests、python3 写的一个 api接口自动化测试框架，后期会将 接入ui自动化。该接口自动化框架的思想与 ui 自动化测试框架的思想一致，使用po模式，分为page业务层、case数据层，以及base层。
2. 该框架的数据源为yml文件，上手有一定的难度。该框架在后期维护的时候，非常方便，只需要简单的维护yml文件即可。
3. 最后的报告是使用allure
4. 本框架已在本公司深度使用，通过jenkins持续集成，每天会定时执行，向测试小组发送执行结果，也会不定期的巡检公司生产环境的功能等。
5. 该框架后期还会有多个分支版本，这些多分支版本均是来优化和提升执行效率的。
6. 该框架目前支持接口数据的依赖，无论是响应结果的数据依赖，还是请求参数值的依赖，均支持。
7. 该框架支持token依赖，由于公司的业务很复杂，角色及流程分支我，所以在实现框架的时候，token的依赖，可以每个case有单独的登录获取token，也可以每个class下使用一个账号登录获取token。所以在使用这块时，有点绕，没法像一些互联网公司那样简单明了。
8. 在进行断言的时候，本框架只支持 pytest.assume() 和 hamcrest().断言表达式里有可能使用到jsonpath的语法，所以对使用者的要求较高。当时可以自己下载本框架进行修改。
9. 由于本人能力有限，该框架里会存在着bug，也是在所难免的。


#### 软件架构
略


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  使用的时候，需要维护三个yml文件。分别为config.yml（在config目录下），page业务层和case数据层 对应的yml文件，如下图所示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0102/113659_4b263340_7622878.png "屏幕截图.png")
2.  config.yml 为总开关，如控制着运行环境的切换等。如我们的环境分别：测试环境、测试公网环境以及生产环境。在切换环境的时候，只需要更改host的值即可。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0102/114118_cff68aa4_7622878.png "屏幕截图.png")
3.  使用的时候，需要更改登录的参数。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0102/115640_f06181a7_7622878.png "屏幕截图.png")
4.  断言表达式里，有时候会使用到jsonpath表达式，不会的同学，需要自己补习一下。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0102/115905_1a7bdb8e_7622878.png "屏幕截图.png")
5.  下图为公司上的jenkins构建的结果报告图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0102/122431_3ae1f14a_7622878.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0102/122650_ac45e00b_7622878.png "屏幕截图.png")

#### 操作文档
1. 请参照 docs 目录下的 word文档《接口测试框架操作手册--可公开.docx》。该文档以及 根目录下的 case_yml_template.yml 和 page_yml_template.yml均对各个字段有详情的说明
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

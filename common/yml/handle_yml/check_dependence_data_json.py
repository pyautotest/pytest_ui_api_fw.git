#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Create Time: 2021/1/15 上午9:55
@Author: guo
@File：check_dependence_data_json.py
"""
import pytest

from common.yml.get_yml_keys import GetYmlKeys
from config.get_conf_data import GetConfData
from common.util.get_file_path import GetFilePath


class CheckJson:
    """
    当dependence_type的值为json时,要检测jsonfilepath的正确性.当书写不正确时,要进行修正.
    书写正确后,要检测该json文件是否存在,不存在,则直接将case置为fail
    """

    def __init__(self):
        self.__conf = GetConfData()
        self.__cfile = GetFilePath()

    def checkdata(self,page_apidata:dict,ymlkeys:GetYmlKeys):
        """
        校验jsonfilepath的合法性,当书写不正确的时候,会进行自动修正,同时还会检测文件是否存在. \n
        当文件不存在时,会直接将case置为fail,当文件存在后,则返回文件的绝对路径.  \n
        书写不正确的情况如下: \n
        json文件的顶层目录为jsonfile,当值为 jsonfile/qingjia/query.json ,此时就是书写不正确. \n
        此时会进行去掉jsonfile/路径.修正为: qingjia/query.json \n
        :param page_apidata:
        :param ymlkeys:
        :return:
        """

        api_data = page_apidata
        yml_keys = ymlkeys


        key = yml_keys.get_jsonfilepath_key()
        dep_case_data_key = yml_keys.get_dependence_case_data_key()
        key_value_tmp = api_data[dep_case_data_key][key]

        # 进行书写校验,同时返回绝对路径
        key_value = self.__cfile.get_file_abspath_json(key_value_tmp)
        # 检测该文件是否存在,若不存在,将当前的case置为fail
        flag = self.__cfile.check_file_exists(key_value)

        if flag == False:
            pytest.fail(f"在case层传入的jsonfilepath的值为: {key_value_tmp},经过书写校验修正后,获取的绝对路径为:\n"
                        f"{key_value} \n"
                        f"检测绝对路径是否存在,结果为: {flag} .说明文件不存在,有可能是书写错误.\n"
                        f"注意: 在书写jsonfilepath时,要写该文件在jsonfile目录下的相对路径,不要包括jsonfile目录.")
        # 表示文件存在.
        else:

            return key_value







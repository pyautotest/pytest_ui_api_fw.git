#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Create Time: 2021/1/14 下午3:46
@Author: guo
@File：handle_dependence_login_data.py
"""
from common.yml.get_yml_keys import GetYmlKeys
from config.get_conf_data import GetConfData


class HandleDependenceLogin:
    """"""
    def __init__(self):
        self.__conf = GetConfData()

    def get_dependence_login(self,page_api_data:dict,yml_keys:GetYmlKeys,case_api_data:dict=None,req_keys:list=None)->dict:
        """
        处理是否有login依赖,默认是没有登录依赖.并进行处理.  \n
        :param page_api_data: 对应的page层的yml里的 api_data 的数据
        :param yml_keys: yml的keys
        :param case_api_data: 传入的case层 yml数据 对应的 api_data的数据
        :param req_keys: 传入的case层的yml数据 对应的key
        :return: dict
        """
        api_data = page_api_data
        yml_keys = yml_keys
        req_params = case_api_data
        req_keys = req_keys

        flag = False
        dependence_key = yml_keys.get_dependence_login_key()
        login_data_key = yml_keys.get_login_data_key()
        key_value = None
        # 需要先判断是否会入了 request_params,当没有传入时 req_keys = None
        # 当req_keys =None时，if key in req_keys 会报错
        if req_keys != None:
            if dependence_key in req_keys:
                key_value = req_params[dependence_key]

        # 以case层的数据为准备,不再判断page层的数据.
        if key_value == True:
            flag = True
        # 直接赋值,无需再返回
        api_data[dependence_key] = flag
        # 当为false时,直接对login_data 赋值为None
        if flag == False:
            api_data[login_data_key] = None

        # 进入到下面的判断,说明req_keys !=None
        else:
            """第一步,提取page层的login_type,防止case层将login_type的值写成空的,然后case层的数据将page层的数据替换后,无login_type的值"""
            login_type_key = yml_keys.get_login_type_key()
            page_login_type = None
            # 先检测 req_keys 是否存在 login_data_key.若不存在,则不进行后续的动作.
            if login_data_key in req_keys:
                # 先获取page层的login_type的值
                page_login_type = api_data[login_data_key][login_type_key]

                """第二步,将case层的login_data 直接赋值给 page层的 login_data"""
                # api_data[login_data_key] = req_params[login_data_key]
                # 为了防止出现case层没写一些关键字段(如login_type),进行导致执行时出现错误,可以通过for循环来获取case层的值.
                req_logindata:dict = req_params[login_data_key]
                # 使用for循环赋值的方式,就不会出现key不存在的情况.
                for key in req_logindata.keys():
                    api_data[login_data_key][key] = req_logindata[key]

                """第三步,检测login_type的值是否为app或web.当不为这些时,要将之前提取的page层的login_type 再赋值回来."""
                login_type = api_data[login_data_key][login_type_key]
                web_str = self.__conf.get_web_str()
                app_str = self.__conf.get_app_str()

                if login_type == None or not isinstance(login_type,str) :
                    api_data[login_data_key][login_type_key] = page_login_type

                # 判断一下 赋值后的login_type的值是否为 app 或 web.当不为这两者之一时,还原为 page层的login_type的值
                elif (app_str not in login_type) and (web_str not in login_type):
                    api_data[login_data_key][login_type_key] = page_login_type

                """第四步,检测login_scope的值. 现在不需要这个字段,所以去掉了相关的逻辑."""

                """第五步,处理token_filepath的值."""
                """后续会根据实际的情况考虑:在执行登录依赖时,保存登录的响应结果"""
                token_path_key = yml_keys.get_token_filepath_key()
                token_path = api_data[login_data_key][token_path_key]

                if token_path!=None:
                    from common.util.get_file_path import GetFilePath
                    fpath = GetFilePath()
                    token_path = fpath.get_file_abspath_json(token_path)
                # 进行赋值
                api_data[login_data_key][token_path_key] = token_path

        # 返回值
        return api_data

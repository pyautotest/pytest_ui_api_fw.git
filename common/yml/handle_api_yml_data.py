#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：handle_api_yml_data.py
"""
import sys
sys.path.append("../../")

from common.yml.get_yml_keys import GetYmlKeys
from common.yml.handle_yml.handle_ensure_run import HandleEnsureRun
from common.yml.handle_yml.handle_request_data import HandleRequestData
from common.yml.handle_yml.handle_dependence_login_data import HandleDependenceLogin
from common.yml.handle_yml.handle_dependence_data import HandleDependenceData
from common.yml.handle_yml.handle_save_response import HandleSaveResponse
from common.yml.handle_yml.handle_check_data import HandleCheckData
from common.yml.handle_yml.handle_assert import HandleAssert

class HandleYmlData:

    def __init__(self,page_yml_data:dict,case_yml_data:dict=None):
        """
        需要传入 page层对应的yml数据(获取后的),在必要时 还要传入case层对应的yml数据(获取后的)  \n
        :param page_yml_data: page层对应的yml数据
        :param case_yml_data: case层对应的yml数据,case层控制着请求参数等.
        """
        self.__ymldata = page_yml_data
        self.__req_params = None
        self.__yml_keys = GetYmlKeys()
        self.__req_keys = None
        if case_yml_data !=None:
            self.__req_params = case_yml_data[self.__yml_keys.get_api_data_key()]

        self.__api_data = self.__ymldata[self.__yml_keys.get_api_data_key()]

        if self.__req_params !=None:
            self.__req_keys = []
            for key in self.__req_params.keys():
                self.__req_keys.append(key)

        self.__ensureRun = HandleEnsureRun()
        self.__reqData = HandleRequestData()
        self.__depLogin = HandleDependenceLogin()
        self.__depData = HandleDependenceData()
        self.__saveRes = HandleSaveResponse()
        self.__checkData = HandleCheckData()
        self.__Assert = HandleAssert()


    def get_handle_yml_data(self)->dict:
        """"""
        # 获取是否运行
        run_key = self.__yml_keys.get_run_whether_key()
        flag = self.__ensureRun.get_ensure_run(self.__api_data,self.__yml_keys,self.__req_params,self.__req_keys)
        self.__api_data[run_key] = flag


        # 获取处理后的request_data的值
        request_key = self.__yml_keys.get_request_data_key()
        request_data = self.__reqData.get_request_data(self.__api_data,self.__yml_keys,self.__req_params,self.__req_keys)
        self.__api_data[request_key] = request_data


        # 获取处理后的dependence_login 及logn_data的值.
        dependence_login_key = self.__yml_keys.get_dependence_login_key()
        logindata_key = self.__yml_keys.get_login_data_key()
        api_data = self.__depLogin.get_dependence_login(self.__api_data, self.__yml_keys,self.__req_params, self.__req_keys)
        self.__api_data[dependence_login_key] = api_data[dependence_login_key]
        self.__api_data[logindata_key] = api_data[logindata_key]


        # 获取处理后的dependence_case 及dependence_case_data的值.
        api_data = self.__depData.get_dependence_data(self.__api_data, self.__yml_keys,self.__req_params, self.__req_keys)
        dep_case_key = self.__yml_keys.get_dependence_case_key()
        dep_case_data_key = self.__yml_keys.get_dependence_case_data_key()
        # self.__api_data = api_data
        self.__api_data[dep_case_key] = api_data[dep_case_key]
        self.__api_data[dep_case_data_key] = api_data[dep_case_data_key]


        # 获取处理后的ensure_save_response 及 save_response的值
        ensure_res_key = self.__yml_keys.get_ensure_save_response_key()
        save_res_key = self.__yml_keys.get_save_response_key()
        api_data = self.__saveRes.get_save_response(self.__api_data, self.__yml_keys,self.__req_params, self.__req_keys)
        self.__api_data[ensure_res_key] = api_data[ensure_res_key]
        self.__api_data[save_res_key] = api_data[save_res_key]


        # 获取处理后的ensure_check 及 check_data的值.
        api_data = self.__checkData.get_check_data(self.__api_data, self.__yml_keys,self.__req_params, self.__req_keys)
        ensure_check_key = self.__yml_keys.get_ensure_check_key()
        checkdata_key = self.__yml_keys.get_check_data_key()
        self.__api_data[ensure_check_key] = api_data[ensure_check_key]
        self.__api_data[checkdata_key] = api_data[checkdata_key]

        # 获取assert 断言
        assert_value = self.__Assert.get_assert(self.__api_data, self.__yml_keys,self.__req_params, self.__req_keys)
        assert_key = self.__yml_keys.get_assert_key()
        self.__api_data[assert_key] = assert_value

        return self.__ymldata

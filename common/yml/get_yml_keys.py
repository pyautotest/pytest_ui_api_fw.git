#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guozg
@File：get_yml_keys.py
"""
from common.yml.yml_keys_mapping import YmlKeys


class GetYmlKeys(YmlKeys):
    '''
    获取yml文件里的key，不包括config.yml文件的key。
    '''

    def get_host_key(self) -> str:
        '''获取yml文件中host所对应的key'''
        return str.strip(self.host_key)

    def get_path_key(self) -> str:
        '''获取yml文件中path所对应的key'''
        return str.strip(self.path_key)

    def get_method_key(self) -> str:
        '''获取yml文件中method所对应的key'''
        return str.strip(self.method_key)

    def get_url_key(self) -> str:
        '''获取yml文件中url所对应的key'''
        return str.strip(self.url_key)

    def get_type_key(self) -> str:
        '''获取type所对应的key'''
        return str.strip(self.type_key)

    def get_api_data_key(self) -> str:
        '''获取yml文件中api_data所对应的key'''
        return str.strip(self.api_data_key)

    def get_run_whether_key(self) -> str:
        '''获取yml文件中run_whether所对应的key'''
        return str.strip(self.run_whether_key)

    def get_data_type_key(self) -> str:
        '''获取yml文件中data_type 所对应的key'''
        return str.strip(self.data_type_key)

    def get_request_data_key(self) -> str:
        '''获取yml文件中 request_data 所对应的key'''
        return str.strip(self.request_data_key)

    def get_dependence_login_key(self) -> str:
        '''获取yml文件中 dependence_login 所对应的key'''
        return str.strip(self.dependence_login_key)

    def get_token_filepath_key(self) ->str:
        """获取yml文件中 token_filepath 所对应的key"""
        return str.strip(self.token_filepath_key)

    def get_login_type_key(self) -> str:
        '''获取yml文件中 login_type 所对应的key'''
        return str.strip(self.login_type_key)

    def get_login_data_key(self) -> str:
        '''获取yml文件中 login_data 所对应的key'''
        return str.strip(self.login_data_key)

    def get_dependence_case_key(self) -> str:
        '''获取yml文件中 dependence_case 所对应的key'''
        return str.strip(self.dependence_case_key)

    def get_dependence_case_data_key(self) -> str:
        '''获取yml文件中 dependence_case_data 所对应的key'''
        return str.strip(self.dependence_case_data_key)

    def get_dependence_case_id_key(self) -> str:
        '''获取yml文件中所依赖的 case_id 所对应的key'''
        return str.strip(self.case_id_key)

    def get_dependence_key(self) -> str:
        '''获取yml文件中 dependence_key 所对应的key'''
        return str.strip(self.dependence_key)

    def get_replace_key(self) -> str:
        '''获取yml文件中 replace_key 所对应的key'''
        return str.strip(self.replace_key)

    def get_loginid_key(self) -> str:
        '''获取yml文件中 loginId 所对应的key'''
        return str.strip(self.loginId_key)

    def get_username_key(self) -> str:
        '''获取yml文件中 username 所对应的key'''
        return str.strip(self.username_key)

    def get_password_key(self) -> str:
        '''获取yml文件中 password 所对应的key'''
        return str.strip(self.password_key)

    # *******************************分割线，后续再添加的字段对应的方法放在下面*****************************
    def get_login_scope_key(self) -> str:
        '''获取yml文件中 login_scope_key 所对应的key'''
        return str.strip(self.login_scope_key)

    def get_dependence_type_key(self) -> str:
        '''获取yml文件中 dependence_type 所对应的key'''
        return str.strip(self.dependence_type_key)

    def get_jsonfilepath_key(self)->str:
        """获取yml文件中 jsonfilepath 所对应的key"""
        return str.strip(self.jsonfilepath_key)

    def get_sqlstatement_key(self)->str:
        """获取yml文件中 sqlstatement 所对应的key"""
        return str.strip(self.sqlstatement_key)


    # ***************************** assert 断言 **************************
    def get_assert_key(self) -> str:
        '''获取yml(主要是case对应的yml文件)中的 assert 所对应的key'''
        return str.strip(self.assert_key)

    # *************************check data 以及保存response 相关的**********************************
    def get_ensure_save_response_key(self) -> str:
        """获取yml文件中 ensure_save_response 所对应的key"""
        return str.strip(self.ensure_save_response_key)

    def get_save_response_key(self) -> str:
        """获取yml文件中 save_response 所对应的key"""
        return str.strip(self.save_response_key)

    def get_save_type_key(self) -> str:
        """获取yml文件中 save_type 所对应的key"""
        return str.strip(self.save_type_key)

    def get_response_filepath_key(self) -> str:
        """获取yml文件中 response_filepath 所对应的key"""
        return str.strip(self.response_filepath_key)

    def get_ensure_check_key(self) -> str:
        """获取yml文件中 ensure_check 所对应的key"""
        return str.strip(self.ensure_check_key)

    def get_check_data_key(self) -> str:
        """获取yml文件中 check_data 所对应的key"""
        return str.strip(self.check_data_key)

    def get_ensure_return_key(self) -> str:
        """获取yml文件中 ensure_return 所对应的key"""
        return str.strip(self.ensure_return_key)

    def get_save_jsonfile_path_key(self) -> str:
        """获取yml文件中 save_jsonfile_path 所对应的key"""
        return str.strip(self.save_jsonfile_path_key)

    def get_save_keys_key(self) -> str:
        """获取yml文件中 save_keys 所对应的key"""
        return str.strip(self.save_keys_key)

    def get_query_keys_key(self) -> str:
        """获取yml文件中 query_keys 所对应的key"""
        return str.strip(self.query_keys_key)

    def get_amount_key(self) -> str:
        """获取yml文件中 amount 所对应的key"""
        return str.strip(self.amount_key)

    def get_expect_data_key(self) -> str:
        """获取yml文件中 expect_data 所对应的key"""
        return str.strip(self.expect_data_key)

    def get_actual_data_key(self) -> str:
        """获取yml文件中 actual_data 所对应的key"""
        return str.strip(self.actual_data_key)

    def get_ensure_checktime_key(self) -> str:
        """获取yml文件中 ensure_checktime 所对应的key"""
        return str.strip(self.ensure_checktime_key)

    def get_time_delta_key(self) -> str:
        """获取yml文件中 time_delta 所对应的key"""
        return str.strip(self.time_delta_key)

    def get_time_type_key(self) -> str:
        """获取yml文件中 time_type 所对应的key"""
        return str.strip(self.time_type_key)

    def get_timedelta_value_key(self) -> str:
        """获取yml文件中 timedelta_value 所对应的key"""
        return str.strip(self.timedelta_value_key)

    def get_create_time_key(self) -> str:
        """获取yml文件中 create_time 所对应的key"""
        return str.strip(self.create_time_key)


if __name__ == '__main__':
    aa = GetYmlKeys()
    # print(aa.get_host_key())
    # print(aa.get_path_key())
    # print(aa.get_type_key())
    # print(aa.get_dependence_case_data_key())
    # print(aa.get_loginid_key())
    # print(aa.get_ensure_checktime_key())
    print(aa.get_ensure_save_response_key())
    print(aa.get_save_response_key())
    print(aa.get_save_type_key())
    print(aa.get_response_filepath_key())
    print(aa.get_ensure_check_key())
    print(aa.get_check_data_key())
    print(aa.get_ensure_return_key())
    print(aa.get_save_jsonfile_path_key())
    print(aa.get_save_keys_key())
    print(aa.get_query_keys_key())
    print(aa.get_amount_key())
    print(aa.get_expect_data_key())
    print(aa.get_actual_data_key())
    print(aa.get_ensure_checktime_key())
    print(aa.get_time_delta_key())
    print(aa.get_time_type_key())
    print(aa.get_timedelta_value_key())
    print(aa.get_create_time_key())
    print("------------------------")
    print(aa.get_dependence_type_key())
    print(aa.get_jsonfilepath_key())
    print(aa.get_sqlstatement_key())
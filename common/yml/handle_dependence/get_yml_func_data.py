#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：get_yml_func_data.py
"""
from common.util.get_file_path import GetFilePath
from common.yml.read_yml_file import ReadYml
from config.get_conf_data import GetConfData


class GetFuncData:
    """获取case层或page层对应的yml文件的数据"""
    def __init__(self):
        self.__conf = GetConfData()
        self.__fpath = GetFilePath()
        self.__ryml = ReadYml()


    def get_yml_func_data(self, case_str: str) -> dict:
        """获取case层或page层对应的yml文件的数据"""

        # 获取case(page)的yml文件及yml文件中case(page) 对应的key值
        yml_join_func_flag = self.__conf.get_yml_join_func()
        caselist = case_str.split(yml_join_func_flag)
        # case/page 所在yml文件(可能含有路径,但不应包括yml文件所在的顶层目录data)
        case_yml = caselist[0]
        case_name = caselist[1]
        # 获取case/page yml的路径
        # case_yml_path = self.__fpath.get_file_abspath_yml(self.yml_dir, case_yml)
        case_yml_path = self.__fpath.get_file_abspath_yml(case_yml)
        # 获取case/page 对应的yml文件数据
        case_req_data = self.__ryml.get_yml_func_data(case_yml_path, case_name)

        return case_req_data
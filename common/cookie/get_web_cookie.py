#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：get_web_cookie.py
"""
import requests


class GetWebCookie:
    '''
    主要是获取token(主要是我们的系统web端返回的只有token)，并将token提取后，存放到cookie或header里  \n
    或者直接返回token({login:"xxxxxxxxxx"})
    '''

    def __init__(self, response: requests.models.Response):
        '''
        要传入接口request的响应结果  \n
        :param response: 响应结果
        '''
        self.__req = response
        self.__cookie = None
        self.__header = None
        self.__token = None
        self.__get_cookies()

    def __get_cookies(self) -> None:
        '''
        获取cookie \n
        :return: None
        '''
        cookie = None
        header = None
        token = None
        if self.__req.status_code == 200:
            cookies = self.__req.cookies
            cookie = {}
            header = {}
            token = {}
            token["login"] = cookies.get('login')
            for key, item in cookies.items():
                cookie[key] = item
                header["Cookie"] = key + "=" + item

        self.__cookie = cookie
        self.__header = header
        self.__token = token

    def get_cookie(self) -> dict:
        '''
        获取cookie，主要是token，并返回  \n
        :return: cookie,dict格式
        '''
        return self.__cookie

    def get_header_cookie(self) -> dict:
        '''
        获取cookie，将值存入到header里，并返回  \n
        :return: header,dict格式
        '''
        return self.__header

    def get_token(self) -> dict:
        '''
        获取token,格式为{login:"xxxxxxxxxx"}  \n
        :return: login,dict格式
        '''
        return self.__token
